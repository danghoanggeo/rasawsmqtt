import context
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import time
import json

HOSTNAME = "ec2-18-233-158-223.compute-1.amazonaws.com"
SENSOR_TOPIC = "temp/sensor1"
LIGHT_ONOFF = "light/onoff"
LIGHT_STATUS = "light/status"
currentLighFlag = 0

data_on = '{"onOff":1,"time":1000}'
data_off = '{"onOff":0,"time":0}'

def convertDataToJson(data):
    fr = data.index("{")
    en = data.index("}") + 1
    str = data[fr:en]
    jdata = j.loads(str)
    return jdata

def showLightStatus(str):
    jdata = convertDataToJson(st)
    print("Status: %s"%(jdata['status']))

def on_connect(mqttc,obj,flags,rc):
    print("rc: " + str(rc))

def on_message(mqttc,obj,msg):
    if(msg.topic == LIGHT_STATUS):
        showLightStatus(str(msg.payload))


def on_publish(mqttc,obj,mid):
    print("mind: " + str(mid))

def on_subscribe(mqttc,obj,mid,granted_qos):
    print("Subscribed: "+ str(mid)+" "+str(granted_qos))

def on_log(mqttc, obj,level, string):
    print(string)

def publishTopic(topic,data):
    publish.single(topic,data,hostname = HOSTNAME)
    print("published!")


mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

mqttc.connect("ec2-18-233-158-223.compute-1.amazonaws.com",1883,60)
mqttc.loop_start()  # start loop to process received messages
mqttc.subscribe(LIGHT_STATUS,0)

print("1")
publishTopic(LIGHT_ONOFF,data_on)
time.sleep(30)
publishTopic(LIGHT_ONOFF,data_off)


mqttc.disconnect() # disconnect
mqttc.loop_stop() # stop loop
