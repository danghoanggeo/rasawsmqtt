import context
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json as j
import re
import time

HOSTNAME = "ec2-18-233-158-223.compute-1.amazonaws.com"
SENSOR_TOPIC = "temp/sensor1"
LIGHT_ONOFF = "light/onoff"
LIGHT_STATUS = "light/status"

def publishTopic(topic,data):
    publish.single(topic,data,hostname = HOSTNAME)

def convertDataToJson(data):
    fr = data.index("{")
    en = data.index("}") + 1
    str = data[fr:en]
    jdata = j.loads(str)
    return jdata

def sensorData(str):
    jdata = convertDataToJson(st)
    print("EC: %s, ORP: %s"%(jdata['EC'],jdata['ORP']))

def lightOnOff(str):
    jdata = convertDataToJson(str)
    print("ONOFF: %s, time: %d"%(jdata['onOff'],jdata['time']))
    # TODO: on off Light
    status = {"status":jdata['onOff']}
    publishTopic(LIGHT_STATUS,status)

def on_connect(mqttc,obj,flags,rc):
    print("rc: " + str(rc))

def on_message(mqttc,obj,msg):
    if(msg.topic == SENSOR_TOPIC):
        sensorData(str(msg.payload))
    elif(msg.topic == LIGHT_ONOFF):
        lightOnOff(str(msg.payload))

def on_publish(mqttc,obj,mid):
    print("mind: " + str(mid))

def on_subscribe(mqttc,obj,mid,granted_qos):
    print("Subscribed: "+ str(mid)+" "+str(granted_qos))

def on_log(mqttc, obj,level, string):
    print(string)

mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe

mqttc.connect("ec2-18-233-158-223.compute-1.amazonaws.com",1883,60)
mqttc.subscribe(SENSOR_TOPIC,0)
mqttc.subscribe(LIGHT_ONOFF,0)

mqttc.loop_forever()
