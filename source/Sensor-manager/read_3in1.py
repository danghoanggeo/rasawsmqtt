#!/usr/bin/env python

#Read sensor 3in1 (pH, temperature, ORP)
#Editor: H.TRONG
#Updated: 2018/10/9, 火
##############################################################
#This code's using minimalmodbus library. For more infomation:
#https://minimalmodbus.readthedocs.io/en/master/readme.html
#https://github.com/pyhys/minimalmodbus


import time

class Sensor3IN1:

    import minimalmodbus
    
    minimalmodbus.BAUDRATE = 9600
    minimalmodbus.BYTESIZE = 8
    minimalmodbus.PARITY = 'N'
    minimalmodbus.STOPBITS = 1
    minimalmodbus.TIMEOUT = 1
    minimalmodbus.CLOSE_PORT_AFTER_EACH_CALL = True
    minimalmodbus.MODE_RTU


    def read_data(self):
        
        """Read data from 3IN1 Sensor. Read detail info in datasheet and minimalmodbus docs
           return [Temp, pH, ORP]
        """
    #Connecting port name and slave ID (in decimal)
    #pH+temp(slave ID = 1)
        self.pH_temp = self.minimalmodbus.Instrument('/dev/ttyUSB0', 1)
    #ORP(slave ID = 2)
        self.ORP_val = self.minimalmodbus.Instrument('/dev/ttyUSB0', 2)
        time.sleep(0.2)
    #Read data
        self.pH_temp_data = self.pH_temp.read_registers(0, 9, 4)            #Read pH, Temperature from register 0
        self.data = [self.pH_temp_data[0]/100, self.pH_temp_data[8]/10]
        time.sleep(0.2)        
        self.data.append(self.ORP_val.read_register(4, 0, 4, True))         #Read ORP from register 4
        return self.data
