#!/usr/bin/env python

#Read EC sensor: Analog EC Meter SKU:DFR0300 (DFrobot)
#Editor: H.TRONG
#Updated: 2018/10/9, 火
import os
import time
import Adafruit_ADS1x15

class ReadEC:


#Initial ADC (ADS1115)
    def __init__(self) -> None:
        self.adc = Adafruit_ADS1x15.ADS1115()   #Init ADS1115


#Read temperature from Thermalsensor DS18B20
#Return Celsius Degree
    def read_Thermal(self) -> float:
        for i in os.listdir('/sys/bus/w1/devices'):
            if i != 'w1_bus_master1':
                self.ds18b20 = i
        self.location = '/sys/bus/w1/devices/' + self.ds18b20 + '/w1_slave'
        with open (self.location) as self.tfile:
            self.tfile.readline()   #Inorge line 1
            self.temperaturedata = self.tfile.readline().split(" ")[9] #Read temperature info at line 2
            self.temperature = self.temperaturedata[2:]
        return(float(self.temperature) / 1000.0)


#Voltage range by gain (ADS1115)
    def voltrange(self, gain = 1):
        if gain == 2/3:
            return(6.144)
        if gain == 1:
            return(4.096)
        if gain == 2:
            return(2.048)
        if gain == 4:
            return(1.024)
        if gain == 8:
            return(0.512)
        if gain == 16:
            return(0.256)

        
#Calculate EC from ADC value with coefficient
#Formular from DFrobot website:
#https://www.dfrobot.com/wiki/index.php/Analog_EC_Meter_SKU:DFR0300
    def read_EC(self, channel = 0, gain = 1) -> float:
        self.temp = self.read_Thermal()
        self.adc_val = self.adc.read_adc(channel, gain)
        self.maxvolt = self.voltrange(gain)
    #Calculate coefficent
        self.EC_value = self.adc_val*1000*self.maxvolt/32767
        self.tempCoefficient=1.0+0.0185*(self.temp-25.0)
        self.adjusted_EC=self.EC_value/self.tempCoefficient
    #Out of mearsure range
        if(self.adjusted_EC<150):
            self.EC_output = 0
        elif(self.adjusted_EC>3300):
            self.EC_output = 0
    #In mearsure range
        else:
            if(self.adjusted_EC<=448):
                self.EC_output=6.84*self.adjusted_EC-64.32
            elif(self.adjusted_EC<=1457):
                self.EC_output=6.98*self.adjusted_EC-127
            else:
                self.EC_output=5.3*self.adjusted_EC+2278
        return(self.EC_output/1000.0)
